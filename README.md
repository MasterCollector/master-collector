
![)](https://i.imgur.com/0JJCyir.png)
# Master Collector 
A small portable assembly downloader (via torrent)

---
# Introduction
Master Collector is a very small and portable application designed for simplifying, downloading, and installing Adobe Apps. It has everything needed to download, install, activate, uninstall, and clean.

# Prerequisites
 - .NET Framework 4.7.2 or Higher
 - Internet Explorer 11 or higher
 - Windows 7 or higher

# Recommended

 - VPN not needed, but essential for protecting your anonymity and privacy.
 - Installing on Windows 7 is possible, but is recommended to use Windows 10 to avoid any complications.

# Usage Guide
1. Download the application from provided link, than launch it.
2. In the application, find your desired app in ```Apps```, and click ```Download``` From there you can select what version to install. Some apps have "Multiple Pages". 
3. Find the version, platform, and year you want and click ```Download```.
4. Picotorrent will open up and prompt you where to store the files.
5. After download has been complete, open the folder in which it was download, open the ```` ISO````, then run the ```Autoplay.exe```

# Additional Information

 - If not properly installed or not installed. The needed .NET Framework will be offered for download.   

You can download the Framework from the official Microsoft website: [.NET Framework 4.7.2](https://dotnet.microsoft.com/download/dotnet-framework/net472)

# Check-sums (Hash)
Important Note:

 - It is really important you verify that you have downloaded the   
   correct file. Do not use if the checksums mismatch!

Here is a simple way to verify the check-sum:

 1. Go to this website: http://onlinemd5.com/
 2. Drag in or select the downloaded Master Collector.exe
 3. Compare the generated value and the one below!

#### MD5:
```83869880ec78317fa8b4f5636ed5ba0d```

# Versions & Changes
Brand new version released `2.1.0` !

**Additions**:
 - Fixed Adobe Acrobat Description
 - Made it optional to use provided torrent client
 - User now selects where to store the torrent file
 - Minor bug fixes
 
**Upcoming Additions**:
 - Add additional options and features for Adobe products
# Author
Assembled and reimagined by TEKKNYKLIFE
# Download

Remember to verify checksum!
https://anonfile.com/efl3v582n8/Master_Collecter_exe